from __future__ import division
import os
import numpy as np
import pandas as pd


def read_nodefrac(nodefrac_f):
    """
    Reads a node-fraction output file from T-COMP
    :param nodefrac_f:
    :return:
    """
    data = []
    with open(nodefrac_f, 'r') as f:
        while True:
            line = f.readline()
            if line == '':
                break
            else:
                shortname = line.split()[0]
                numrecs = int(f.readline().split()[0])
                while numrecs > 0:
                    node, frac, junk, area, vfrac, fracmaxarea, trans = f.readline().split()
                    data.append([shortname, int(node), float(frac), float(area),
                                 float(vfrac), float(fracmaxarea), float(trans)])
                    numrecs -= 1
    colnames = ['name', 'node', 'frac', 'area', 'vfrac', 'fracmaxarea', 'trans']
    df = pd.DataFrame(data=data, columns=colnames).set_index(['name', 'node'], drop=False)
    return df


def write_nodefrac(df, filename):
    dfcols = ['name', 'node', 'frac', 'area', 'vfrac', 'fracmaxarea', 'trans']
    for c in dfcols:
        assert c in df.columns.tolist(), 'Column {} not found in the input DataFrame'.format(c)
    header = '{:0>10d}  !! Fraction                   Area        Vert.Fraction    Frac.Max.Area    Cell Trans.'
    with open(filename, 'w') as f:
        for name in df.name.unique():
            dfi = df[df.name == name].copy()
            numrecs = len(dfi)
            f.write('{}\n'.format(name))
            f.write('{}\n'.format(header.format(numrecs)))
            for idx, row in dfi.iterrows():
                items = list([str(v) for v in row.values[1:]])
                items.insert(2, '!!')
                f.write('\t'.join(items) + '\n')
    return

if __name__ == '__main__':
    in_f = r'E:\modflow\fasgwmod\model\fas_SS.2.1\Pcomp_mmusess.Tcomp_Node-Fractions.txt'
    nf = read_nodefrac(in_f)
    print(len(nf))
    gb = nf.groupby(['name'])['frac'].sum()
    nf = nf[nf.name.isin(gb[gb > 0].index)]
    print(len(nf))
    # out_f = r'E:\modflow\fasgwmod\ancillary\tcomp\test\text_frac.txt'
    write_nodefrac(nf, in_f)

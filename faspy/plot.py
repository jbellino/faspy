# from __future__ import print_function
# # suppress annoying pandas openpyxl warning
# import warnings
# warnings.filterwarnings('ignore', category=UserWarning)
import numpy as np
import copy
import os
from faspy.utils import roundup
from mpl_toolkits.basemap import pyproj
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm, Colormap
import matplotlib.ticker as ticker
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from scipy.stats import gaussian_kde

# Set some default font parameters to be applied to all figures.
# See http://matplotlib.org/users/customizing.html for a complete guide to the rc parameter defaults.
# from faspy.rcparams import *


fas_proj4 = '+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-84 +x_0=0 +y_0=0\
             +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs'
default_parallels = np.arange(20., 36., 2.5)
default_meridians = np.arange(-90., -74., 2.5)


class ModelBaseMap(object):

    def __init__(self, llcrnrlon=-88.4, llcrnrlat=24.05, urcrnrlon=-78., urcrnrlat=35.4,
                 proj='aea', lat_0=23., lon_0=-84., lat_1=29.5, lat_2=45.5, show_bbox=False, resolution='h',
                 draw_parallels=True, draw_meridians=True, draw_basemap_shps=True, filled=True, lay=None,
                 parallels=default_parallels, meridians=default_meridians, **kwargs):
        if 'ax' in kwargs:
            self.ax = kwargs.pop('ax')
        else:
            try:
                self.ax = plt.gca()
                self.ax.set_aspect('equal')
            except:
                self.ax = plt.subplot(1, 1, 1, aspect='equal', axisbg="white")
        self.lay = lay
        self.proj = proj
        self.shppath = r'E:\modflow\fasgwmod\gis\shp\nad83'
        self.basemap = Basemap(llcrnrlon=llcrnrlon, llcrnrlat=llcrnrlat,
                               urcrnrlon=urcrnrlon, urcrnrlat=urcrnrlat,
                               lat_0=lat_0, lon_0=lon_0, lat_1=lat_1, lat_2=lat_2,
                               resolution=resolution, projection=self.proj, ellps='GRS80', **kwargs)

        # self.basemap = Basemap(llcrnrlon=llcrnrlon, llcrnrlat=llcrnrlat,
        #                        urcrnrlon=urcrnrlon, urcrnrlat=urcrnrlat,
        #                        lat_0=lat_0, lon_0=lon_0, lat_1=lat_1, lat_2=lat_2,
        #                        resolution=resolution, projection=self.proj, ellps='GRS80', ax=self.ax, **kwargs)

        # # draw basemap features
        # self.shppath = r'\\IGSBABEWGS020\modflow\GIS\shp\nad83'
        # if draw_basemap_shps:
        #     self.draw_basemap_shapes(self, filled=filled)
        #
        # if show_bbox:
        #     self.draw_bbox()
        #
        # # draw parallels
        # if draw_parallels:
        #     self.basemap.drawparallels(parallels, linewidth=0., labels=[1, 0, 0, 0], fmt=lat2str)
        #
        # # draw meridians
        # if draw_meridians:
        #     self.basemap.drawmeridians(meridians, linewidth=0., labels=[0, 0, 0, 1], fmt=lon2str)
        #
        # self.ax.get_xaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
        # self.ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
        return

    def draw_basemap(self, show_bbox=False, draw_parallels=True, draw_meridians=True,
                     draw_basemap_shps=True, filled=True, parallels=default_parallels,
                     meridians=default_meridians, **kwargs):

        if 'ax' in kwargs:
            ax = kwargs.pop('ax')
        else:
            try:
                ax = plt.gca()
                ax.set_aspect('equal')
            except:
                ax = plt.subplot(1, 1, 1, aspect='equal', axisbg="white")

        # if draw_basemap_shps:
            # self.draw_counties(filled=filled)
            # self.draw_states()
            # self.draw_fallline()
            # self.draw_extent()
            # self.draw_lakeokeechobee(filled=filled)

        # if show_bbox:
        #     self.draw_bbox()

        # draw parallels
        if draw_parallels:
            self.basemap.drawparallels(parallels, linewidth=0.1, labels=[1, 0, 0, 0], fmt=lat2str)

        # draw meridians
        if draw_meridians:
            self.basemap.drawmeridians(meridians, linewidth=0.1, labels=[0, 0, 0, 1], fmt=lon2str)

        ax.get_xaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
        ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
        return

    def draw_shp(self, shp, name, linewidth=0.4, color='black', zorder=4, **kwargs):
        self.basemap.readshapefile(shp, name, linewidth=linewidth, color=color, zorder=zorder, **kwargs)

    def draw_lines(self, lineshp, layer_name, **kwargs):
        self.basemap.readshapefile(lineshp, layer_name, **kwargs)

    def draw_polys(self, polyshp, layer_name, filled=True, **kwargs):
        self.basemap.readshapefile(polyshp, layer_name, **kwargs)
        if filled:
            patches = []
            for info, shape in zip(self.basemap.counties_info, self.basemap.counties):
                patches.append(Polygon(np.array(shape), True))
                self.ax.add_collection(PatchCollection(patches, facecolor='lightgray', linewidths=0., zorder=1))

    # def draw_counties(self, shp=None, filled=True, linewidth=0.2, color='gray', zorder=3, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'Counties_pol')
    #     self.basemap.readshapefile(shp, 'counties', linewidth=linewidth, color=color, zorder=zorder, **kwargs)
    #     if filled:
    #         patches = []
    #         for info, shape in zip(self.basemap.counties_info, self.basemap.counties):
    #             patches.append(Polygon(np.array(shape), True))
    #             self.ax.add_collection(PatchCollection(patches, facecolor='lightgray', linewidths=0., zorder=1))
    #     return self

    # def draw_states(self, shp=None, linewidth=0.4, color='black', zorder=4, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'fourstates2m')
    #     self.basemap.readshapefile(shp, 'states', linewidth=linewidth, color=color, zorder=zorder, **kwargs)
    #
    # def draw_fallline(self, shp=None, linewidth=0.2, color='black', zorder=5, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'fallline_ver_algasc')
    #     self.basemap.readshapefile(shp, 'fall_line', linewidth=linewidth, color=color, zorder=zorder, **kwargs)
    #
    # def draw_extent(self, shp=None, linewidth=0.4, color='blue', zorder=5, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'fas')
    #     self.basemap.readshapefile(shp, 'fas_extent', linewidth=linewidth, color=color, zorder=zorder, **kwargs)
    #
    # def draw_bbox(self, shp=None, linewidth=0.4, color='blue', zorder=6, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'grid_bbox')
    #     self.basemap.readshapefile(shp, 'grid_bbox', linewidth=linewidth, color=color, zorder=zorder, **kwargs)
    #
    # def draw_lakeokeechobee(self, shp=None, filled=True, linewidth=0.4, zorder=6, **kwargs):
    #     if shp is None:
    #         shp = os.path.join(self.shppath, 'lake_okeechobee')
    #     self.basemap.readshapefile(shp, 'lake_o', linewidth=linewidth, color='blue', zorder=zorder, **kwargs)
    #     if filled:
    #         patches = []
    #         for info, shape in zip(self.basemap.lake_o_info, self.basemap.lake_o):
    #             patches.append(Polygon(np.array(shape), True))
    #             self.ax.add_collection(PatchCollection(patches, facecolor='cyan', linewidths=0., zorder=12))

    def plot_scatter(self, x, y, **kwargs):
        if 'ax' in kwargs:
            ax = kwargs.pop('ax')
        else:
            ax = self.ax
        if isinstance(x, list):
            x = np.array(x)
        if isinstance(y, list):
            y = np.array(y)
        pathcollection = ax.scatter(x, y, **kwargs)
        return pathcollection

    def plot_residuals(self, x, y, residuals, break_value=0, size_factor=None, **kwargs):
        """
        :param x: numpy.ndarrray
        :param y: numpy.ndarray
        :param residuals: numpy.ndarray
            Residual values
        :param break_value:
        :param size_factor:
        :param kwargs:
            Keyword arguments passed to matplotlib.pyplot.scatter()
        :return:
        """
        if 'ax' in kwargs:
            ax = kwargs.pop('ax')
        else:
            ax = self.ax
        if isinstance(x, list):
            x = np.array(x)
        if isinstance(y, list):
            y = np.array(y)
        if isinstance(residuals, list):
            residuals = np.array(residuals)
        if size_factor is None:
            size_factor = 1./np.mean(np.abs(residuals))
        over_idx = np.where(residuals > break_value)
        under_idx = np.where(residuals < break_value)
        pathcollection_over = ax.scatter(x[over_idx], y[over_idx], s=np.abs(residuals[over_idx]) * size_factor,
                                         c='b', zorder=100, label='Residual-positive', **kwargs)
        pathcollection_under = ax.scatter(x[under_idx], y[under_idx], s=np.abs(residuals[under_idx]) * size_factor,
                                          c='r', zorder=100, label='Residual-negative', **kwargs)
        return pathcollection_over, pathcollection_under

    def plot_array(self, x, y, z, masked_values=None, log=False, **kwargs):
        """
        Plot an array.  If the array is three-dimensional, then the method
        will plot the layer tied to this class (self.lay).
        Parameters
        ----------
        x : numpy.ndarray
            Array of x-coordinates in projected units
        y : numpy.ndarray
            Array of y-coordinates in projected units
        z : numpy.ndarray
            Array to plot.
        masked_values : iterable of floats, ints
            Values to mask.
        **kwargs : dictionary
            keyword arguments passed to matplotlib.pyplot.pcolormesh
        Returns
        -------
        quadmesh : matplotlib.collections.QuadMesh
        """
        if 'ax' in kwargs:
            ax = kwargs.pop('ax')
        else:
            ax = self.ax
        if z.ndim == 3 and self.lay is not None:
            plotarray = z[self.lay-1, :, :]
        elif z.ndim == 2:
            plotarray = z
        else:
            raise Exception('Array must be of dimension 2 or "layer" must be set in the ModelBaseMap object.')
        if masked_values is not None:
            for mval in masked_values:
                plotarray = np.ma.masked_equal(plotarray, mval)
        if log:
            norm = LogNorm(vmin=z.min().min(), vmax=z.max().max())
        else:
            norm = None
        # convert fas_aea to map_aea
        fas_aea = pyproj.Proj(fas_proj4)
        map_aea = pyproj.Proj(self.basemap.proj4string)
        x, y = pyproj.transform(fas_aea, map_aea, x / 3.2808, y / 3.2808)
        quadmesh = self.basemap.pcolormesh(x, y, plotarray, norm=norm, ax=ax, **kwargs)
        return quadmesh

    def contour_array(self, x, y, z, levels=None, masked_values=None, **kwargs):
        """
        Contour an array.  If the array is three-dimensional, then the method
        will contour the layer tied to this class (self.lay).
        Parameters
        ----------
        x : numpy.ndarray
            Array of x-coordinates in projected units
        y : numpy.ndarray
            Array of y-coordinates in projected units
        z : numpy.ndarray
            Array to plot.
        masked_values : iterable of floats, ints
            Values to mask.
        **kwargs : dictionary
            keyword arguments passed to matplotlib.pyplot.pcolormesh
        Returns
        -------
        contour_set : matplotlib.pyplot.contour
        """
        if z.ndim == 3 and self.lay is not None:
            plotarray = z[self.lay-1, :, :]
        elif z.ndim == 2:
            plotarray = z
        else:
            raise Exception('Array must be of dimension 2 or "layer" must be set in the ModelBaseMap object.')
        if masked_values is not None:
            if not isinstance(masked_values, list):
                masked_values = list(masked_values)
            for mval in masked_values:
                plotarray = np.ma.masked_equal(plotarray, mval)
        if 'ax' in kwargs:
            ax = kwargs.pop('ax')
        else:
            ax = self.ax
        if 'colors' in kwargs.keys():
            if 'cmap' in kwargs.keys():
                cmap = kwargs.pop('cmap')
            cmap = None
        # convert fas_aea to map_aea
        fas_aea = pyproj.Proj(fas_proj4)
        map_aea = pyproj.Proj(self.basemap.proj4string)
        x, y = pyproj.transform(fas_aea, map_aea, x / 3.2808, y / 3.2808)
        contour_set = ax.contour(x, y, plotarray, levels=levels, **kwargs)
        return contour_set

    def tick_label_formatter_comma_sep(x, pos):
        return '{:,.0f}'.format(x)

    def get_proj4_vars(proj4):
        """
        Reads a proj4 string and returns the values
        :param proj4:
        :return: [proj, lat_1, lat_2, lat_0, lon_0, x_0, y_0, ellps, towgs84, units]
        """
        vars = [s.split('=')[1] for s in proj4.split(' ')[:-1]]
        for i in range(len(vars)):
            try:
                vars[i] = float(vars[i])
            except:
                pass
        return vars


def tick_label_formatter_comma_sep(x, pos):
    return '{:,.0f}'.format(x)


def get_proj4_vars(proj4):
    """
    Reads a proj4 string and returns the values
    :param proj4:
    :return: [proj, lat_1, lat_2, lat_0, lon_0, x_0, y_0, ellps, towgs84, units]
    """
    vars = [s.split('=')[1] for s in proj4.split(' ')[:-1]]
    for i in range(len(vars)):
        try:
            vars[i] = float(vars[i])
        except:
            pass
    return vars


def lat2str(deg):
    min = 60 * (deg - np.floor(deg))
    deg = np.floor(deg)
    dir = 'N'
    if deg < 0:
        if min != 0.0:
            deg += 1.0
            min -= 60.0
        dir = 'S'
    return u"{}\N{DEGREE SIGN} {:0>2d}'".format(int(np.abs(deg)), int(np.abs(min)))


def lon2str(deg):
    min = 60 * (deg - np.floor(deg))
    deg = np.floor(deg)
    dir = 'E'
    if deg > 180:
        deg -= 360.
        if min != 0.0:
            deg += 1.0
            min -= 60.0
        dir = 'W'
    return u"{}\N{DEGREE SIGN} {:0>2d}'".format(int(np.abs(deg)), int(np.abs(min)))


if __name__ == '__main__':
    import flopy
    loadpth = r'C:\Users\jbellino\Documents\Project\floridan\model\testing\Zeta2dtest'
    ml = flopy.modflow.Modflow.load('swiex1.nam', model_ws=loadpth)
    zeta_f = os.path.join(loadpth, 'swiex1.zta')
    zta = zeta2d(ml, zeta_f)
    print(zta[0].shape)
    print(np.count_nonzero(np.isnan(zta[0])))



def plot_basemap_old(x, y, z=None, llcrnrlon=-88.4, llcrnrlat=24.05, urcrnrlon=-78., urcrnrlat=35.4,
                 proj='aea', lat_0=23., lon_0=-84., lat_1=29.5, lat_2=45.5, show_bbox=False, save_filename=None,
                 resolution='h', colorbar=True, log=False, show=True, contour_interval=None, title=None,
                 colormesh=True, shppath=r'\\IGSBABEWGS020\modflow\GIS\shp\nad83', **kwargs):
    import mpl_toolkits.basemap.pyproj as pyproj
    from mpl_toolkits.basemap import Basemap
    if isinstance(contour_interval, float):
        contour_interval = int(contour_interval)

    if save_filename is not None:
        if not os.path.isdir(os.path.dirname(save_filename)):
            os.makedirs(os.path.dirname(save_filename))

    if 'figsize' in kwargs:
        figsize = kwargs.pop('figsize')
    else:
        figsize = None

    fig, ax = plt.subplots(figsize=figsize, **kwargs)
    basemap = Basemap(llcrnrlon=llcrnrlon, llcrnrlat=llcrnrlat,
                  urcrnrlon=urcrnrlon, urcrnrlat=urcrnrlat,
                  lat_0=lat_0, lon_0=lon_0, lat_1=lat_1, lat_2=lat_2,
                  resolution=resolution, projection=proj, ellps='GRS80', ax=ax)

    fas_aea = pyproj.Proj('+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-84 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=ft +no_defs')
    map_aea = pyproj.Proj(basemap.proj4string)

    # map.drawmapboundary(fill_color='lightgray')

    # Convert fas_aea to map_aea
    x, y = pyproj.transform(fas_aea, map_aea, x/3.2808, y/3.2808)
    norm = None

    if z is not None:
        z = np.ma.masked_where(np.isnan(z), z)
        if log:
            norm = LogNorm(vmin=z.min().min(), vmax=z.max().max())

        if contour_interval is not None:
            ctour = basemap.contour(x, y, z, levels=range(roundup(z.min(), contour_interval),
                                                      roundup(z.max(), contour_interval),
                                                      contour_interval),
                                norm=norm, linewidths=0.6, linestyles='solid', colors='k', zorder=7)
            plt.clabel(ctour, inline=1, fontsize=10, fmt='%d')

        if colormesh:
            pcm = basemap.pcolormesh(x, y, z, norm=norm, zorder=2) # , cmap=Colormap('viridis')
        else:
            pcm = basemap.contourf(x, y, z, norm=norm, zorder=2) # , cmap=Colormap('viridis')
        if colorbar:
            basemap.colorbar(pcm, "right", size="5%", pad='2%')

    # Draw basemap features
    # shppath = os.path.join(gispath, 'shp', 'nad83')
    basemap.readshapefile(os.path.join(shppath, 'Counties_pol'), 'counties', linewidth=0.2, color='gray', zorder=3)
    basemap.readshapefile(os.path.join(shppath, 'fourstates2m'), 'states', linewidth=0.4, color='black', zorder=4)
    basemap.readshapefile(os.path.join(shppath, 'fallline_ver_algasc'), 'fall_line', linewidth=0.2, color='black', zorder=5)
    basemap.readshapefile(os.path.join(shppath, 'fas'), 'fas_extent', linewidth=0.4, color='blue', zorder=5)
    if show_bbox:
        basemap.readshapefile(os.path.join(shppath, 'grid_bbox'), 'grid_bbox', linewidth=0.4, color='blue', zorder=6)
    # basemap.readshapefile(os.path.join(shppath, 'lake_okeechobee'), 'lake_o', linewidth=0.4, drawbounds=False, zorder=6)

    # parallels
    parallels = np.arange(20., 36., 2.5)
    basemap.drawparallels(parallels, linewidth=0., labels=[1,0,0,0], fmt=lat2str)
    # draw meridians
    meridians = np.arange(-90., -74., 2.5)
    basemap.drawmeridians(meridians, linewidth=0., labels=[0,0,0,1], fmt=lon2str)

    patches = []
    for info, shape in zip(basemap.counties_info, basemap.counties):
        patches.append(Polygon(np.array(shape), True))
    ax.add_collection(PatchCollection(patches, facecolor='lightgray', linewidths=0., zorder=1))

    # patches = []
    # for info, shape in zip(basemap.lake_o_info, basemap.lake_o):
    #     patches.append(Polygon(np.array(shape), True))
    # ax.add_collection(PatchCollection(patches, facecolor='c', edgecolor='blue', linewidths=.2, zorder=2))

    # for pt in pts:
    #     for k, v in pt.iteritems():
    #         if k == 'name':
    #             name = v
    #         elif k == 'feat':
    #             if v.endswith('.shp'):
    #                 v = v.split('.')[0]
    #             feat = v
    #         elif k == 'marker':
    #             marker = v
    #         elif k == 'markersize':
    #             markersize = v
    #         elif k == 'color':
    #             color = v
    #     pt_info = basemap.readshapefile(feat, name)
        # print pt_info
        # for info, pt in zip(basemap.pt_info, basemap.name):
        #     basemap.plot(name[0], name[1], marker=marker, color=color, markersize=markersize, markeredgewidth=0)

    ax.get_xaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
    ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))

    if title is not None:
        plt.title(title)

    if show:
        plt.show()
    if save_filename is not None:
        fig.savefig(save_filename)
    plt.close()
    return


def plot_image(a, title='', fname=None, figsize=None, fmt='%.0f', show=True, log=False, x_axis_log=False,
               y_axis_log=False):

    fig, ax = plt.subplots(1, 1, figsize=figsize)

    ax.set_title(title)
    ax.set_ylabel('Row')
    ax.set_xlabel('Column')
    if log:
        im = ax.matshow(a, norm=LogNorm(vmin=a.min(), vmax=a.max()), aspect='equal')
    else:
        im = ax.imshow(a, aspect='equal')

    # Create colorbar in the appended axes
    # Tick locations can be set with the kwarg `ticks`
    # and the format of the ticklabels with kwarg `format`
    cbar = plt.colorbar(im, format=fmt)

    if x_axis_log:
        ax.set_xscale('log')

    if y_axis_log:
        ax.set_yscale('log')

    plt.tight_layout()
    if fname is not None:
        fig.savefig(fname)
    if show:
        plt.show()
        plt.close()
    return


def plot_images(arrays=[], titles=[], plotrows=1, plotcols=None, figsize=None, mask_value=None, fname=None,
                fmt='%.0f', show=True, log=False, x_axis_log=False, y_axis_log=False):

    if plotcols is None:
        plotcols = len(arrays)

    fig, axes = plt.subplots(plotrows, plotcols, figsize=figsize)
    ims = np.empty(len(arrays), np.object)
    dividers = np.empty(len(arrays), np.object)
    caxs = np.empty(len(arrays), np.object)
    cbars = np.empty(len(arrays), np.object)
    axes = axes.reshape(-1)
    for idx, (a, t) in enumerate(zip(arrays, titles)):
        axes[idx].set_title(t)
        axes[idx].set_ylabel('Row')
        axes[idx].set_xlabel('Column')
        if mask_value is not None:
            a = np.ma.masked_where(a==mask_value, a)
        if log:
            ims[idx] = axes[idx].matshow(a, norm=LogNorm(vmin=a.min(), vmax=a.max()), aspect='equal')
        else:
            ims[idx] = axes[idx].imshow(a, aspect='equal')
        dividers[idx] = make_axes_locatable(axes[idx])
        # Append axes to the right of ax1, with 20% width of ax1
        caxs[idx] = dividers[idx].append_axes("right", size="2.5%", pad=0.05)
        # Create colorbar in the appended axes
        # Tick locations can be set with the kwarg `ticks`
        # and the format of the ticklabels with kwarg `format`
        cbars[idx] = plt.colorbar(ims[idx], cax=caxs[idx], format=fmt)

        if x_axis_log:
            axes[idx].set_xscale('log')

        if y_axis_log:
            axes[idx].set_yscale('log')

    plt.tight_layout()
    if fname is not None:
        plt.savefig(fname)
    if show:
        plt.show()
        plt.close()
    return


def plot3D(z, mask=None, colormap='gist_earth', warp_scale=0.02, fname=None, figsize=(400, 350)):
    from mayavi import mlab
    fig = mlab.figure(size=figsize, bgcolor=(0.16, 0.28, 0.46))
    if type(z) == np.ndarray:
        z = [np.copy(z)]
    else:
        z = np.copy(z)
    for idx, j in enumerate(z):
        if mask is not None:
            if type(mask) != np.ndarray and np.isnan(mask):
                mask = np.ma.masked_where(np.isnan(j), j).mask
            elif type(mask) == float or type(mask) == int:
                mask = np.ma.masked_where(j==mask, j).mask

        j[np.isnan(j)] = j[~np.isnan(j)].min()
        mlab.surf(j, mask=None, colormap=colormap, warp_scale=warp_scale)

    if fname is not None:
        mlab.savefig(fname, size=figsize, figure=fig)
    mlab.show()
    return


def plot_xsec(x, y, z, surface_colors=False, lines=False, line_colors=False, labels=False, fname=False,
                     figsize=None, ylabel=False, xlabel=False, title=False):
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    if title:
        plt.title(title)
    if ylabel:
        ax.set_ylabel(ylabel)
    if ylabel:
        ax.set_ylabel(xlabel)
    ax.set_xlim([min(x), max(x)])
    color_cycle = ax._get_lines.color_cycle
    if surface_colors:
        c = copy.deepcopy(surface_colors)
        if type(c) == np.ndarray:
            c = c.tolist()

    surface_pairs = [(s, z[idx+1]) for idx, s in enumerate(z) if idx<len(z)-1]

    for (s1, s2) in surface_pairs:
        if surface_colors:
            facecolor=next(color_cycle)
        else:
            facecolor=c.pop(0)
        ax.fill_between(x, s1[x, y], s2[x, y], facecolor=facecolor)
    if lines:
        for idx, line in enumerate(lines):
            if not line_colors:
                line_color = None
            else:
                line_color = line_colors[idx]
            plt.plot(line[x, y], color=line_color)
    plt.tight_layout()
    if fname:
        plt.savefig(fname)
    plt.show()
    plt.close()


def one2one(observed, simulated, kde=True, log=False,
            xlabel='Observed', ylabel='Simulated', **kwargs):
    """
    make a one to one plot
    Taken from Applied Groundwater-Modeling GitHub repo, chapter 9 and 10 problems, functions.py
    by rjhhunt@facstaff.wisc.edu at https://github.com/Applied-Groundwater-Modeling-2nd-Ed
    """
    if 'ax' in kwargs:
        ax = kwargs.pop('ax')
    else:
        ax = plt.gca()

    if kde:
        # Calculate the point density
        xy = np.vstack([observed, simulated])
        z = gaussian_kde(xy)(xy)

        # Sort the points by density, so that the densest points are plotted last
        idx = z.argsort()
        x, y, z = observed[idx], simulated[idx], z[idx]

    else:
        x, y, z = observed, simulated, None

    # scatter plot of observed vs. simulated
    scatter_plot = ax.scatter(x, y, c=z, edgecolor='')

    if log:
        ax.set_yscale('log')
        ax.set_xscale('log')

    # Draw the 1st-order polynomial fit line
    fit = np.polyfit(x, y, 1)
    fit_fn = np.poly1d(fit)
    ax.plot(x, fit_fn(x), '--k')

    # plot one to one line
    lims = ax.get_ylim() + ax.get_xlim()
    mn, mx = np.min(lims), np.max(lims)

    ax.plot(np.arange(mn, mx+1), np.arange(mn, mx+1), color='k')
    ax.set_xlim(mn, mx)
    ax.set_ylim(mn, mx)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    return scatter_plot


def volumetric_budget_bar_plot(values_in, values_out, labels, **kwargs):
    if 'ax' in kwargs:
        ax = kwargs.pop('ax')
    else:
        ax = plt.gca()

    x_pos = np.arange(len(values_in))
    rects_in = ax.bar(x_pos, values_in, align='center', alpha=0.5)

    x_pos = np.arange(len(values_out))
    rects_out = ax.bar(x_pos, values_out, align='center', alpha=0.5)

    plt.xticks(list(x_pos), labels)
    ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
    ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))

    ymin, ymax = ax.get_ylim()
    if ymax != 0:
        if abs(ymin) / ymax < .33:
            ymin = -(ymax * .5)
        else:
            ymin *= 1.35
    else:
        ymin *= 1.35
    plt.ylim([ymin, ymax * 1.25])

    for i, rect in enumerate(rects_in):
        label = '{:,.0f}'.format(values_in[i])
        height = values_in[i]
        x = rect.get_x() + rect.get_width() / 2
        y = height + (.02 * ymax)
        vertical_alignment = 'bottom'
        horizontal_alignment = 'center'
        ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)

    for i, rect in enumerate(rects_out):
        label = '{:,.0f}'.format(values_out[i])
        height = values_out[i]
        x = rect.get_x() + rect.get_width() / 2
        y = height + (.02 * ymin)
        vertical_alignment = 'top'
        horizontal_alignment = 'center'
        ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)

    # horizontal line indicating zero
    ax.plot([rects_in[0].get_x() - rects_in[0].get_width() / 2,
             rects_in[-1].get_x() + rects_in[-1].get_width()], [0, 0], "k")

    return rects_in, rects_out


def mflist_bar_plot(values_in, values_out, x_labels, **kwargs):
    from flopy.utils import MfListBudget

    if 'ax' in kwargs:
        ax = kwargs.pop('ax')
    else:
        ax = plt.gca()

    x_pos = np.arange(len(values_in))
    rects_in = ax.bar(x_pos, values_in, align='center', alpha=0.5)

    x_pos = np.arange(len(values_out))
    rects_out = ax.bar(x_pos, values_out, align='center', alpha=0.5)

    plt.xticks(list(x_pos), x_labels)
    ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
    ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))

    ymin, ymax = ax.get_ylim()
    plt.ylim([ymin * 1.35, ymax * 1.25])

    for i, rect in enumerate(rects_in):
        label = '{:,.0f}'.format(values_in[i])
        height = values_in[i]
        x = rect.get_x() + rect.get_width() / 2
        y = height + (.02 * ymax)
        vertical_alignment = 'bottom'
        horizontal_alignment = 'center'
        ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)

    for i, rect in enumerate(rects_out):
        label = '{:,.0f}'.format(values_out[i])
        height = values_out[i]
        x = rect.get_x() + rect.get_width() / 2
        y = height + (.02 * ymin)
        vertical_alignment = 'top'
        horizontal_alignment = 'center'
        ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)

    # horizontal line indicating zero
    ax.plot([rects_in[0].get_x() - rects_in[0].get_width() / 2,
             rects_in[-1].get_x() + rects_in[-1].get_width()], [0, 0], "k")

    return rects_in, rects_out


# def mflist_bar_plot(list_file, mult=1, start_datetime=None, **kwargs):
#     from flopy.utils import MfListBudget
#
#     if 'ax' in kwargs:
#         ax = kwargs.pop('ax')
#     else:
#         ax = plt.gca()
#
#     inc_df, cum_df = MfListBudget(list_file).get_dataframes(start_datetime=start_datetime)
#     cum_df *= mult
#     s = cum_df.T.squeeze()
#
#     ins_df = s.loc[list([i for i in s.index if i.endswith('_IN')])]
#     x_pos = np.arange(len(ins_df))
#     values_in = ins_df.values
#     rects_in = ax.bar(x_pos, values_in, align='center', alpha=0.5)
#
#     out_df = s.loc[list([i for i in s.index if i.endswith('_OUT')])] * -1
#     x_pos = np.arange(len(out_df))
#     values_out = out_df.values
#     rects_out = ax.bar(x_pos, values_out, align='center', alpha=0.5)
#
#     mb_df = s.loc[['IN-OUT', 'PERCENT_DISCREPANCY']]
#     start = x_pos[-1] + 1
#     stop = start + len(mb_df)
#     mb_x_pos = np.arange(start, stop)
#     values_mb = mb_df.values
#     rects_mb = ax.bar(mb_x_pos, values_mb, align='center', alpha=0.5)
#
#     x_labels = list([i[:-3] for i in ins_df.index]) + list([i for i in mb_df.index])
#     plt.xticks(list(x_pos) + list(mb_x_pos), x_labels)
#     ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
#     ax.get_yaxis().set_major_formatter(mpl.ticker.FuncFormatter(tick_label_formatter_comma_sep))
#
#     ymin, ymax = ax.get_ylim()
#     plt.ylim([ymin * 1.35, ymax * 1.25])
#
#     for i, rect in enumerate(rects_in):
#         label = '{:,.0f}'.format(values_in[i])
#         height = values_in[i]
#         x = rect.get_x() + rect.get_width() / 2
#         y = height + (.02 * ymax)
#         vertical_alignment = 'bottom'
#         horizontal_alignment = 'center'
#         ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)
#
#     for i, rect in enumerate(rects_out):
#         label = '{:,.0f}'.format(values_out[i])
#         height = values_out[i]
#         x = rect.get_x() + rect.get_width() / 2
#         y = height + (.02 * ymin)
#         vertical_alignment = 'top'
#         horizontal_alignment = 'center'
#         ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)
#
#     for i, rect in enumerate(rects_mb):
#         label = '{:,.0f}'.format(values_mb[i])
#         height = values_out[i]
#         x = rect.get_x() + rect.get_width() / 2
#         if height >= 0:
#             y = height + (.02 * ymax)
#             vertical_alignment = 'bottom'
#         else:
#             y = height + (.02 * ymin)
#             vertical_alignment = 'top'
#         horizontal_alignment = 'center'
#         ax.text(x, y, label, ha=horizontal_alignment, va=vertical_alignment, rotation=90)
#
#     # horizontal line indicating zero
#     ax.plot([rects_in[0].get_x() - rects_in[0].get_width() / 2,
#              rects_mb[-1].get_x() + rects_mb[-1].get_width()], [0, 0], "k")
#
#     return rects_in, rects_out, rects_mb

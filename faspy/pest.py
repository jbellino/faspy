import os
import pandas as pd
from pyemu import pst_utils
from collections import OrderedDict


def pst_add_parameter(pst_obj, tpl_f, par_f, pargpnme, fixed=[], **kwargs):
    # Stage parameter information
    # PARNME PARTRANS PARCHGLIM PARVAL1 PARLBND PARUBND PARGP SCALE OFFSET DERCOM
    pars = OrderedDict()
    parvars = ['parnme', 'partrans', 'parchglim', 'parval1', 'parlbnd',
               'parubnd', 'pargp', 'scale', 'offset', 'dercom']

    df = pd.read_csv(par_f, skiprows=1, names=parvars, delim_whitespace=True)
    for tup in df.itertuples():
        p = OrderedDict([(k, v) for k, v in zip(parvars, tup[1:])])
        parnme = p['parnme']
        if parnme in fixed:
            p['partrans'] = 'FIXED'
        pars[parnme] = p

    # load parameters
    par_data = pst_utils.populate_dataframe(pars.keys(),
                                            pst_obj.par_fieldnames,
                                            pst_obj.par_defaults,
                                            pst_obj.par_dtype)
    for k, d in iter(pars.items()):
        for fieldname, val in iter(d.items()):
            par_data.loc[k, fieldname] = val

    if not hasattr(pst_obj, 'parameter_data'):
        pst_obj.parameter_data = pd.DataFrame(data=None, columns=pst_obj.par_fieldnames)
    if len(pst_obj.parameter_data) == 0:
        pst_obj.parameter_data = pst_obj.parameter_data.append(par_data)
    else:
        for idx in par_data.index:
            pst_obj.parameter_data.loc[idx] = par_data.loc[idx]

    _update_par_group(pst_obj, pargpnme, **kwargs)
    _update_tpl_files(pst_obj, tpl_f)
    return


def pst_add_observations(pst_obj, ins_f, obs_df, out_f=None, write_ins_f=True):
    if write_ins_f:
        with open(ins_f, 'w') as f:
            f.write('pif ~\n')
            f.write('~Simulated~\n')
        with open(ins_f, 'a') as f:
            s = obs_df.obsnme.map('l1 !{}!'.format)
            f.write(s.to_string(index=False, header=False))
    _update_ins_files(pst_obj, ins_f, out_f)

    # load observations
    obs_data = pst_utils.populate_dataframe(obs_df.obsnme,
                                            pst_obj.obs_fieldnames,
                                            pst_obj.obs_defaults,
                                            pst_obj.obs_dtype)
    for c in obs_data.columns:
        obs_data.loc[:, c] = obs_df[c].values
    if not hasattr(pst_obj, 'observation_data'):
        pst_obj.observation_data = pd.DataFrame(data=None, columns=pst_obj.obs_fieldnames)
    # Remove existing observations with the same name
    drop = pst_obj.observation_data.index.intersection(obs_data.index)
    if len(drop) > 0:
        pst_obj.observation_data = pst_obj.observation_data.drop(drop)
    pst_obj.observation_data = pst_obj.observation_data.append(obs_data, verify_integrity=True)
    return


def _update_par_group(pst_obj, pargpnme, inctyp='relative', derinc=1.e-3,
                      derinclb=1.e-5, forcen='switch', derincmul=2., dermthd='parabolic',
                      splitthresh=1.e-5, splitreldiff=5.e-1, splitaction='smaller'):

    # Build empty dataframe
    pargp_data = pst_utils.populate_dataframe([pargpnme],
                                              pst_obj.pargp_fieldnames,
                                              pst_obj.pargp_defaults,
                                              pst_obj.pargp_dtype)

    # Populate dataframe
    pargp_data.loc[pargpnme, 'pargpnme'] = pargpnme
    pargp_data.loc[pargpnme, 'inctyp'] = inctyp
    pargp_data.loc[pargpnme, 'derinc'] = derinc
    pargp_data.loc[pargpnme, 'derinclb'] = derinclb
    pargp_data.loc[pargpnme, 'forcen'] = forcen
    pargp_data.loc[pargpnme, 'derincmul'] = derincmul
    pargp_data.loc[pargpnme, 'dermthd'] = dermthd
    pargp_data.loc[pargpnme, 'splitthresh'] = splitthresh
    pargp_data.loc[pargpnme, 'splitreldiff'] = splitreldiff
    pargp_data.loc[pargpnme, 'splitaction'] = splitaction

    if not hasattr(pst_obj, 'parameter_groups'):
        pst_obj.parameter_groups = pd.DataFrame(data=None, columns=pst_obj.pargp_fieldnames)
    if len(pst_obj.parameter_groups) == 0:
        pst_obj.parameter_groups = pst_obj.parameter_groups.append(pargp_data)
    else:
        for idx in pargp_data.index:
            pst_obj.parameter_groups.loc[idx] = pargp_data.loc[idx]
    return


def _update_tpl_files(pst_obj, f):
    if not hasattr(pst_obj, 'template_files'):
        pst_obj.template_files = []
    if not hasattr(pst_obj, 'input_files'):
        pst_obj.input_files = []
    if f not in pst_obj.template_files:
        pst_obj.template_files.append(f)
        pst_obj.input_files.append(os.path.splitext(f)[0])
    return


def _update_ins_files(pst_obj, f, out_f):
    if out_f is None:
        out_f = os.path.splitext(f)[0] + '.out'
    if not hasattr(pst_obj, 'instruction_files'):
        pst_obj.instruction_files = []
    if not hasattr(pst_obj, 'output_files'):
        pst_obj.output_files = []
    if f not in pst_obj.instruction_files:
        pst_obj.instruction_files.append(f)
        pst_obj.output_files.append(out_f)
    pst_obj.control_data.ninsfle = len(pst_obj.instruction_files)
    return


if __name__ == '__main__':
    from glob import glob
    from pyemu import Pst

    model_ws = r'E:\modflow\fasgwmod\model'

    case = 'junk'
    f_name = os.path.join(model_ws, '{case}.pst'.format(case=case))

    # Wipe out any existing files with the same case
    old = glob(os.path.join(model_ws, '{case}.*'.format(case=case)))
    for f in old:
        os.remove(f)

    pcf = Pst(f_name, load=False)

    # Add baseflow observations

    obs_f = os.path.join(model_ws, 'obs_coords', 'baseflow.dat')
    obs_df = pd.read_csv(obs_f, delim_whitespace=True)
    obs_df.loc[:, 'weight'] = 0.01 * .00001
    obs_df.loc[:, 'obgnme'] = 'bf_a'  # + obs_df.obsnme.str[-1]
    ins_f = os.path.join(model_ws, os.path.basename(os.path.splitext(obs_f)[0] + '.ins'))
    pst_add_observations(pcf, ins_f, obs_df)


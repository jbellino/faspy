import os
import numpy as np
import pandas as pd
from datetime import date
from collections import OrderedDict
"""
Container for model-specific info and functions.
"""


def get_xcenter_array(delx, ncol):
    """
    Return a numpy one-dimensional float array that has the cell center x
    coordinate for every column in the grid in model space - not offset or rotated.
    """
    delx = np.ones(shape=(ncol,), dtype=np.float32) * delx
    x = np.add.accumulate(delx) - 0.5 * delx
    return x


def get_ycenter_array(dely, nrow):
    """
    Return a numpy one-dimensional float array that has the cell center x
    coordinate for every row in the grid in model space - not offset of rotated.
    """
    dely = np.ones(shape=(nrow,), dtype=np.float32) * dely
    Ly = np.add.reduce(dely)
    y = Ly - (np.add.accumulate(dely) - 0.5 * dely)
    return y


def rotate(x, y, xorigin, yorigin, theta):
    theta = -theta * np.pi / 180.
    xrot = xorigin + np.cos(theta) * (x - xorigin) - np.sin(theta) * (y - yorigin)
    yrot = yorigin + np.sin(theta) * (x - xorigin) + np.cos(theta) * (y - yorigin)
    return xrot, yrot


def get_period_ndays(periods_dict, freq):
    # List the months encompassed by each stress period so we can extract
    # the appropriate observation data
    if isinstance(freq, str):
        freq = [freq] * len(periods_dict)
    # nper = len(periods_dict)
    nday = []
    for sp, val in periods_dict.items():
        start, end = periods_dict[sp]['start_datetime'], periods_dict[sp]['end_datetime']
        dates = pd.date_range(start, end, freq=freq[sp])
        idx = pd.DatetimeIndex(start=periods_dict[sp]['start_datetime'],
                               end=periods_dict[sp]['end_datetime'],
                               freq=freq[sp])
        nday_spi = pd.DataFrame(data=None, columns=['period', 'ndays'], index=idx)
        for dt in dates:
            if dt.year in leap_years and dt.month == 2:
                days = 29
            else:
                days = days_dict[dt.month]
            nday_spi.loc['{}-{}'.format(dt.year, dt.month), :] = [sp, days]
        nday.append(nday_spi)
    nday = pd.concat(nday)
    nday = nday.astype(int)
    return nday


leap_years = np.arange(1896, 2011, 4)
days_dict = {1: 31,
             2: 28,
             3: 31,
             4: 30,
             5: 31,
             6: 30,
             7: 31,
             8: 31,
             9: 30,
             10: 31,
             11: 30,
             12: 31}


nrow, ncol, nlay = 680, 680, 9
delx, dely = 5000., 5000.
units = 'feet'
rotation = -22.670566945
proj4_items_dict = OrderedDict([('proj', 'aea'),
                                ('lat_1', 29.5),
                                ('lat_2', 45.5),
                                ('lat_0', 23),
                                ('lon_0', -84),
                                ('x_0', 0),
                                ('y_0', 0),
                                ('ellps', 'GRS80'),
                                ('towgs84', '0,0,0,0,0,0,0'),
                                ('units', 'ft'),
                                ('no_defs', '')])
proj4_items = list(['+{}={}'.format(k, v) for k, v in proj4_items_dict.items() if v != ''])
proj4_items.extend(list(['+{}'.format(k) for k, v in proj4_items_dict.items() if v == '']))
proj4_str = ' '.join(proj4_items)
# proj4_str = ''
# for k, v in iter(proj4_items_dict.items()):
#     if isinstance(v, str) and len(v) == 0:
#         proj4_str += '+{}'.format(k)
#     else:
#         proj4_str += '+{}={} '.format(k, v)

grid_shp = os.path.join('E:', 'modflow', 'georef', 'grid.shp')

ul = (-1777794.3192260002, 3067415.467192)
ll = (-467325.2466162776, -69887.64371925092)
# ur = (1359508.7916852508, 4377884.539801722)
# lr = (2669977.864294973, 1240581.4288904713)

(xul, yul) = ul
(xll, yll) = ll
# (xur, yur) = ur
# (xlr, ylr) = lr

xcenter = get_xcenter_array(delx, ncol)
ycenter = get_ycenter_array(dely, nrow)

[xcentergrid, ycentergrid] = np.meshgrid(xcenter, ycenter)

no_data_val = -999.99

_arc_ascii_header_items = ['NCOLS         {}'.format(ncol),
                           'NROWS         {}'.format(nrow),
                           'XLLCORNER     {}'.format(xul),
                           'YLLCORNER     {}'.format(yul),
                           'CELLSIZE      {}'.format(delx),
                           'NODATA_VALUE  {}'.format(no_data_val)]

arc_ascii_header = '\n'.join(_arc_ascii_header_items) + '\n'


def transform(x, y, xll=xll, yll=yll, theta=rotation):
    x, y = x.copy(), y.copy()
    x += xll
    y += yll
    x, y = rotate(x, y, theta=theta, xorigin=xll, yorigin=yll)
    return x, y

proj_x, proj_y = transform(xcentergrid, ycentergrid)


def layrowcol_from_node(nodes):
    if isinstance(nodes, int):
        nodes = np.array([nodes])
    elif isinstance(nodes, list):
        nodes = np.array(nodes)
    layers = np.zeros(len(nodes), dtype=np.int)
    rows = np.zeros(len(nodes), dtype=np.int)
    columns = np.zeros(len(nodes), dtype=np.int)
    nnodes = nrow * ncol
    for idx, node in enumerate(nodes):
        ilay = 1
        while node > nnodes:
            ilay += 1
            node -= nnodes
        irow = int(node / ncol)
        icol = node - (irow * ncol)
        ilay -= 1
        icol -= 1
        layers[idx] = ilay
        rows[idx] = irow
        columns[idx] = icol
    if len(nodes) == 1:
        return layers[0], rows[0], columns[0]
    else:
        return layers, rows, columns

if __name__ == '__main__':
    print(proj4_items_dict)
    print(xcenter[0])

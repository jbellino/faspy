import os
import sys
import math
import shutil
import numpy as np
from collections import OrderedDict
from ast import literal_eval
from scipy.ndimage import measurements, generic_filter
from glob import glob
from zipfile import ZipFile, ZIP_DEFLATED
from flopy.utils import CellBudgetFile


def open_to_layer(top, bottom, open_top, open_bot, abs_thresh=None, proportion_thresh=None, kx=None, kx_thresh=None):
    """
        Given an open interval, determine which model layers are intersected.

        top : array of floats
            Array of layer tops.
        bottom : array of floats
            Array of layer bottoms.
        open_top : array of floats
            Tops of open interval.
        open_bot : array of floats
            Bottoms of open interval.
        abs_thresh : float
            Minimum absolute difference between edges of open interval
            and layer top or bottom.
        proportion_thresh : float
            Minimum proportional difference between overlap of open interval
            and the layer versus the total open length.
        kx : array of floats (optional)
            Array of hydraulic conductivity at the well location.
        kx_thresh : float (optional)
            Value to use as the minimum hydraulic conductivity when
            determining if a well is open to a layer.

        returns : bool array
        """
    if kx_thresh is not None:
        assert kx is not None, 'Please provide an array of Kx values to compare.'
    if abs_thresh is not None and proportion_thresh is not None:
        print('"abs_thresh" and "proportion_thresh" both set. Defaulting to "abs_thresh".')

    thresh = 0.
    if abs_thresh is not None and proportion_thresh is None:
        thresh = abs_thresh
    elif abs_thresh is None and proportion_thresh is not None:
        thresh = (top - bottom) * proportion_thresh
    elif abs_thresh is not None and proportion_thresh is not None:
        thresh = np.min([abs_thresh, (top - bottom) * proportion_thresh])

    if kx_thresh is not None:
        return (np.greater_equal(open_top, bottom + thresh) &
                np.less_equal(open_bot, top - thresh) &
                np.greater_equal(kx, kx_thresh))
    else:
        return (np.greater_equal(open_top, bottom + thresh) &
                np.less_equal(open_bot, top - thresh))


# def open_interval_layers(open_top, open_bot, tops, bottoms, kx=None, kx_thresh=None):
#     """
#     Given an open interval, determine which model layers are intersected.
#     open_top : float
#         Top of open interval.
#     open_bot : float
#         Bottom of open interval.
#     tops : array of floats
#         Array of layer tops at the well location.
#     bottoms : array of floats
#         Array of layer bottoms at the well location.
#     kx : array of floats (optional)
#         Array of hydraulic conductivity at the well location.
#     kx_thresh : float (optional)
#         Value to use as the minimum hydraulic conductivity when
#         determining if a well is open to a layer.
#
#     returns : bool array
#     """
#     s = 'Length of inputs does not match.'
#     assert len(tops) == len(bottoms) == len(kx), s
#     if open_top > tops[0]:
#         open_top = tops[0]
#     if open_bot < bottoms[-1]:
#         open_bot = bottoms[-1]
#     if kx_thresh is not None:
#         assert kx is not None, 'Please provide an array of Kx values to compare.'
#         open2layers = (np.greater_equal(open_top, bottoms) &
#                        np.less_equal(open_bot, tops) &
#                        np.greater_equal(kx, kx_thresh))
#     else:
#         open2layers = (np.greater_equal(open_top, bottoms) &
#                        np.less_equal(open_bot, tops))
#     return open2layers


def get_transmissivity(k, b, heads=None):

    # get k-values
    paklist = ml.get_package_list()
    if 'LPF' in paklist:
        hk = ml.lpf.hk.array
    elif 'UPW' in paklist:
        hk = ml.upw.hk.array
    else:
        raise ValueError('No LPF or UPW package.')

    if 'DIS' in paklist:
        tops = np.empty_like(ml.dis.botm.array, dtype=float)
        tops[0] = ml.dis.top.array
        tops[1:] = ml.dis.botm.array[:-1]
        botm = ml.dis.botm.array
    else:
        raise ValueError('No DIS package.')

    if heads is not None:
        tops[tops > heads] = heads[tops > heads]

    thick = tops - botm
    thick[thick < 0] = 0
    T = thick * hk
    return T


def zeta2d(ml, zeta_f, text='ZETASRF  1', kstpkper=None, totim=None):
    """
    Flattens 3-dimensional ZWI2 zeta-surface output into a 2-dimensional array.

    Parameters
    ----------
    ml : model object
        A model object (of type :class:`flopy.modflow.mf.Modflow`) with DIS and
        BAS6 packages.
    zeta_f : str or CellBudgetFile object
        Name of the SWI2 output .zta file or a CellBudgetFile instantiated on
        such a file.
    text :  str
        Name of the zeta surface for which the array(s) are desired.
    kstpkper : tuple of ints or list of int tuples
        A tuple containing the time step and stress period (kstp, kper).
        The kstp and kper values are zero based.
    totim : float or list of floats
        The simulation time.
    """
    if isinstance(zeta_f, str):
        assert os.path.isfile(zeta_f), 'Could not find specified zeta file.'
        ztaobj = CellBudgetFile(zeta_f)
    if isinstance(zeta_f, CellBudgetFile):
        ztaobj = zeta_f

    if kstpkper is not None:
        if not isinstance(kstpkper, list):
            kstpkper = [kstpkper]
    elif totim is not None:
        if not isinstance(totim, list):
            totim = [totim]
    else:
        kstpkper = ztaobj.get_kstpkper()

    if kstpkper is not None:
        rawzetalst = [ztaobj.get_data(kstpkper=kk, text=text)[0] for kk in kstpkper]
    elif totim is not None:
        rawzetalst = [ztaobj.get_data(totim=t, text=text)[0] for t in totim]

    tops = np.stack([ml.dis.top.array] + [ml.dis.botm.array[lay] for lay in range(ml.dis.nlay - 1)])
    bottoms = ml.dis.botm.array

    zeta = []

    for rawzeta in rawzetalst:

        # Create an empty container for the processed results
        zetai = np.zeros_like(rawzeta) * np.nan

        # # Mask inactive areas
        # rawzeta = np.ma.masked_where(ml.bas6.ibound.array == 0, rawzeta)

        for lay in range(ml.dis.nlay):

            rzi = rawzeta[lay].copy()

            # Set array to NaN where the zeta surface
            # is equal to the top of the layer (indicates
            # where the seawater/freshwater interface is
            # above the layer), except in layer 1
            if lay > 0:
                rzi[rzi >= tops[lay, :, :]] = np.nan

            # or equal to the bottom of
            # the layer (the opposite case)
            elif lay < ml.dis.nlay:
                rzi[rzi <= bottoms[lay, :, :]] = np.nan

            zetai[lay, :, :] = rzi

        # Transform the 3D array to 2D by averaging along
        # the first (zeroth) axis.
        zetai = np.nanmean(zetai, axis=0)

        # Look for NANs in the 2D array, these indicate
        # that the zeta surface is coincident with a layer boundary
        # which won't have been caught by the above logic.
        r, c = np.where(np.isnan(zetai))
        for rij, cij in zip(r, c):

            # Get a 1D array of the top and bottom elevations
            # and the raw zeta output for this cell
            tij = tops[:, rij, cij]
            bij = bottoms[:, rij, cij]
            zij = rawzeta[:, rij, cij]

            # Iterate down through the layers
            above = False
            below = False
            for tkij, bkij, zkij in zip(tij, bij, zij):
                if zkij == bkij:
                    below = True
                elif zkij == tkij:
                    above = True
                elif tkij > zkij > bkij:
                    zetai[rij, cij] = zkij
                    break
                if above and below:
                    zetai[rij, cij] = tkij
                    break
        zeta.append(zetai)

    return zeta


def ibound_from_zeta(ml, zetafile, text='ZETASRF  1', kstpkper=None):

    assert os.path.isfile(zetafile), 'Zeta file {} does not exist.'.format(zetafile)
    zf = CellBudgetFile(zetafile)

    if kstpkper is None:
        kstpkper = zf.get_kstpkper()[-1]

    zeta = zf.get_data(kstpkper=kstpkper, text=text)[0]
    ibound = np.ones((ml.dis.nlay, ml.dis.nrow, ml.dis.ncol), dtype=int)

    for l in range(ml.dis.nlay):
        # If zeta is equal to or greater than the top of this layer
        # set ibound to 0

        if l == 0:
            top = ml.dis.top.array
        else:
            top = ml.dis.botm.array[l - 1]

        r, c = np.where(zeta[l] >= top)
        ibound[l, r, c] = 0

        # # If zeta is below the top and greater than the centroid
        # # set ibound to 0
        # r, c = np.where((zeta[l] < top) & (zeta[l] >= ml.dis.zcentroids[l]))
        # ibound[l, r, c] = 0
    return ibound


def filter_isolated_cells(a, area_thresh=1, struct=None):
    """ Return array with completely isolated single cells removed
    This was posted as an answer to a question on stackoverflow.com
    http://stackoverflow.com/a/29325216/698809

    :param a: Array with completely isolated single cells
    :param area_thresh: Area threshold to identify when cells are isolated
    :param struct: Structure array for generating unique regions
    :return: Array with minimum region size >= area_thresh

    Example usage
    --------------
    # >>> # Run function on sample array
    # >>> filtered_array = filter_isolated_cells(square, struct=np.ones((3,3)))
    """
    fa = np.copy(a)
    id_regions, num_ids = measurements.label(fa, structure=struct)  # default struct array omits diagonal neighbors
    id_sizes = np.array(measurements.sum(a, id_regions, range(num_ids + 1)))
    area_mask = (id_sizes <= area_thresh)
    fa[area_mask[id_regions]] = 0
    return fa


def roundup(x, k):
    return (int(math.ceil(x / float(k)))) * k


def compute_neighbor_overlap(x, y, t, b):
    thickness = t[x, y] - b[x, y]

    overlap = list()
    # Left side
    olap = thickness
    ti, tj = t[x, y], t[x-1, y]
    bi, bj = b[x, y], b[x-1, y]
    if ti > tj:
        olap -= ti - tj
    if bi < bj:
        olap += bi - bj
    if olap < 0.:
        olap = 0.
    overlap.append(olap/thickness)

    # Right side
    olap = thickness
    ti, tj = t[x, y], t[x+1, y]
    bi, bj = b[x, y], b[x+1, y]
    if ti > tj:
        olap -= ti - tj
    if bi < bj:
        olap += bi - bj
    if olap < 0.:
        olap = 0.
    overlap.append(olap/thickness)

    # Above
    olap = thickness
    ti, tj = t[x, y], t[x, y-1]
    bi, bj = b[x, y], b[x, y-1]
    if ti > tj:
        olap -= ti - tj
    if bi < bj:
        olap += bi - bj
    if olap < 0.:
        olap = 0.
    overlap.append(olap/thickness)

    # Below
    olap = thickness
    ti, tj = t[x, y], t[x, y+1]
    bi, bj = b[x, y], b[x, y+1]
    if ti > tj:
        olap -= ti - tj
    if bi < bj:
        olap += bi - bj
    if olap < 0.:
        olap = 0.
    overlap.append(olap/thickness)
    return overlap


def find_edge(a, z1, z2, full3D=False):
    """
    Finds the edge of zone z1 with respect to contact with zone z2.
    :param a: a, 2-d array of ints
    :param z1: zone 1, int
    :param z2: zone 2, int
    :return: array, bool
    """
    a = np.ma.masked_where((a != z1) & (a != z2), a)

    if len(a.shape) == 2:
        a = np.array([a])

    edges = np.zeros(a.shape, dtype=bool)

    # find zone edges from left to right
    nz = a[:, :, 1:]
    nzl = a[:, :, :-1]
    l, r, c = np.where((nz == z1) & (nzl == z2))
    # adjust column values to account for the starting position of "nz"
    c += 1
    edges[l, r, c] = True

    # find zone edges from right to left
    nz = a[:, :, :-1]
    nzr = a[:, :, 1:]
    l, r, c = np.where((nz == z1) & (nzr == z2))
    edges[l, r, c] = True

    # find zone edges from top to bottom
    nz = a[:, :-1, :]
    nzb = a[:, 1:, :]
    l, r, c = np.where((nz == z1) & (nzb == z2))
    edges[l, r, c] = True

    # find zone edges from bottom to top
    nz = a[:, 1:, :]
    nzt = a[:, :-1, :]
    l, r, c = np.where((nz == z1) & (nzt == z2))
    # adjust row values to account for the starting position of "nz"
    r += 1
    edges[l, r, c] = True

    if full3D:
        # find zone edges from in to out
        # find zone edges from out to in
        print('The full3D option is not supported.')

    if len(a.shape) == 2:
        return edges[0, :, :]
    else:
        return edges


def proportion_of_total_thickness(vals):
    """
    Returns relative proportion of total thickness for a series of layers.
    :param vals: List of values indicating top and bottom of interval as well as surface elevations in between.
    :return:
    """
    vals = sorted(vals, reversed)
    tt = float(vals[0]-vals[-1])
    layer_elevations = vals[1:-2]
    return [i/tt for i in layer_elevations]


def make_grid(ul, delx, dely, nrow, ncol, outpath='.', outbasename='grid.shp', rotation=0., proj4_str=None, esri_proj=None):
    """
    Makes a grid of uniformly spaced cells of the given dimensions.
    vars:
        ul: upper-left coordinate; tuple.
        delx: width of cells in the x-direction; float.
        dely: width of cells in the y-direction; float.
        nrow: number of rows; int.
        ncol: number of cols; int.
        rotation: rotation of grid from North, in degrees; float.
        proj4: output projection of grid; PROJ.4 string.
        esri_proj: ESRI projection file (.prj)
    """
    import fiona
    from fiona.crs import from_string
    from shapely.geometry import LineString, Point, Polygon, MultiPolygon, mapping
    from shapely import affinity
    from pyproj import Proj, transform
    poly_fields = OrderedDict([('node', 'int:9'),
                               ('row', 'int:9'),
                               ('col', 'int:9'),
                               ('X', 'float'),
                               ('Y', 'float'),
                               ('lat', 'float'),
                               ('lon', 'float'),
                               ('area', 'float')])

    pt_fields = OrderedDict([('node', 'int:9'),
                             ('row', 'int:9'),
                             ('col', 'int:9'),
                             ('X', 'float'),
                             ('Y', 'float'),
                             ('lat', 'float'),
                             ('lon', 'float')])

    schema_poly = OrderedDict([('geometry', 'Polygon'),
                               ('properties', poly_fields),
                               ('crs', proj4_str)])

    schema_bbox = OrderedDict([('geometry', 'Polygon'),
                               ('properties', {}),
                               ('crs', proj4_str)])

    schema_line = OrderedDict([('geometry', 'LineString'),
                               ('properties', {}),
                               ('crs', proj4_str)])

    schema_pt = OrderedDict([('geometry', 'Point'),
                             ('properties', pt_fields),
                             ('crs', proj4_str)])

    polygons = list()
    attributes = OrderedDict([(field, list()) for field in ['node', 'row', 'col']])

    print('Building polygons...')
    ul_pt = Point(ul)
    next_ul_pt = ul_pt
    node = 1
    for row in range(nrow):

        for col in range(ncol):

            pts = [(next_ul_pt.x, next_ul_pt.y), (next_ul_pt.x+delx, next_ul_pt.y),
                   (next_ul_pt.x+delx, next_ul_pt.y-dely), (next_ul_pt.x, next_ul_pt.y-dely),
                   (next_ul_pt.x, next_ul_pt.y)]

            polygons.append(Polygon(pts))
            attributes['node'].append(node)
            attributes['row'].append(row+1)
            attributes['col'].append(col+1)

            next_ul_pt = Point(next_ul_pt.x+delx, next_ul_pt.y)
            node += 1

        next_ul_pt = Point(ul_pt.x, ul_pt.y-(dely*(row+1)))

    attributes = dict({k: np.array(v) for k, v in iter(attributes.items())})

    # Rotate the grid (positive rotation is counter-clockwise)
    print('Rotating the grid...')
    rotated_polygons = affinity.rotate(MultiPolygon(polygons), -1*rotation, origin=ul)

    # Get the projected coordinates for the rotated grid
    x_values, y_values, area = list(), list(), list()
    for poly in rotated_polygons:
        x, y = poly.centroid.xy
        x_values.append(x[0])
        y_values.append(y[0])
        area.append(poly.area)

    attributes['X'] = np.array(x_values)
    attributes['Y'] = np.array(y_values)
    attributes['area'] = np.array(area)

    # Compute lat/lon from projection coordinates
    print('Transforming projected coordinates to lat/lon...')
    origin_proj = Proj(from_string(proj4_str), preserve_units=True)
    dest_proj = Proj(init='EPSG:4269')
    lon, lat = transform(origin_proj, dest_proj, attributes['X'], attributes['Y'])
    attributes['lon'] = lon
    attributes['lat'] = lat

    print('Saving coordinate and lat/lon arrays...')
    np.save(os.path.join(outpath, 'proj_x'), np.reshape(attributes['X'], (nrow, ncol)))
    np.save(os.path.join(outpath, 'proj_y'), np.reshape(attributes['Y'], (nrow, ncol)))
    np.save(os.path.join(outpath, 'lat'), np.reshape(attributes['lat'], (nrow, ncol)))
    np.save(os.path.join(outpath, 'lon'), np.reshape(attributes['lon'], (nrow, ncol)))


    print('Writing shapefiles...')
    grid_fname = os.path.join(outpath, outbasename)
    grid_line_fname = "{}_line.shp".format(grid_fname[:-4])
    grid_pt_fname = "{}_pts.shp".format(grid_fname[:-4])
    grid_bbox_fname = "{}_bbox.shp".format(grid_fname[:-4])

    shp = fiona.open(grid_fname, 'w', 'ESRI Shapefile', schema_poly)
    shp_line = fiona.open(grid_line_fname, 'w', 'ESRI Shapefile', schema_line)
    shp_pt = fiona.open(grid_pt_fname, 'w', 'ESRI Shapefile', schema_pt)
    shp_bbox = fiona.open(grid_bbox_fname, 'w', 'ESRI Shapefile', schema_bbox)

    try:

        for idx, poly in enumerate(rotated_polygons):

            shp.write({'properties': OrderedDict([(k, attributes[k][idx]) for k in iter(poly_fields.keys())]),
                           'geometry': mapping(poly)})

            shp_line.write({'properties': {},
                            'geometry': mapping(LineString(poly.boundary))})

            shp_pt.write({'properties': OrderedDict([(k, attributes[k][idx]) for k in iter(pt_fields.keys())]),
                          'geometry': mapping(poly.centroid)})

            # Find upper left corner of bbox
            if attributes['row'][idx] == 1 and attributes['col'][idx] == 1:
                x, y = poly.exterior.coords.xy
                bbox_ul = zip(x, y)[0]

            # Find upper right corner of bbox
            elif attributes['row'][idx] == 1 and attributes['col'][idx] == ncol:
                x, y = poly.exterior.coords.xy
                bbox_ur = zip(x, y)[1]

            # Find lower right corner of bbox
            elif attributes['row'][idx] == nrow and attributes['col'][idx] == ncol:
                x, y = poly.exterior.coords.xy
                bbox_lr = zip(x, y)[2]

            # Find lower left corner of bbox
            elif attributes['row'][idx] == nrow and attributes['col'][idx] == 1:
                x, y = poly.exterior.coords.xy
                bbox_ll = zip(x, y)[3]

        # Append the corners of the bbox in the appropriate order
        bbox_pts = [bbox_ul, bbox_ur, bbox_lr, bbox_ll, bbox_ul]

        # Now that we have all of the corners, write the bbox shapefile
        bbox = Polygon(bbox_pts)
        shp_bbox.write({'properties': {}, 'geometry': mapping(bbox)})


    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(e, exc_type, fname, exc_tb.tb_lineno)

    finally:
        shp.close()
        shp_line.close()
        shp_pt.close()
        shp_bbox.close()

    # Copy ESRI .prj file
    print('Copying ESRI projection information...')
    shutil.copy(esri_proj, "{}.prj".format(grid_fname[:-4]))
    shutil.copy(esri_proj, "{}.prj".format(grid_line_fname[:-4]))
    shutil.copy(esri_proj, "{}.prj".format(grid_pt_fname[:-4]))
    shutil.copy(esri_proj, "{}.prj".format(grid_bbox_fname[:-4]))

    # Write metadata
    print('Writing metadata...')
    outxml = os.path.join(os.path.abspath(os.path.dirname(grid_fname)), 'metadata.xml')
    metadata_dict = OrderedDict()
    metadata_dict['units'] = ('model units', 'feet')
    metadata_dict['ul'] = ('upper-left corner', bbox_ul)
    metadata_dict['ur'] = ('upper-right corner', bbox_ur)
    metadata_dict['lr'] = ('lower-right corner', bbox_lr)
    metadata_dict['ll'] = ('lower-left corner', bbox_ll)
    metadata_dict['delx'] = ('cell-width', delx)
    metadata_dict['dely'] = ('cell-height', dely)
    metadata_dict['nrow'] = ('number of rows', nrow)
    metadata_dict['ncol'] = ('number of columns', ncol)
    metadata_dict['rotation'] = ('grid rotation angle, in degrees from North', rotation)
    metadata_dict['proj4'] = ('proj4 projection string', proj4_str)
    write_grid_metadata(metadata_dict, outxml)


def write_grid_metadata(metadata_dict, outxml):
    from lxml import etree
    root = etree.Element("metadata")
    doc = etree.SubElement(root, "grid")
    for k, (name, text) in metadata_dict.iteritems():
        etree.SubElement(doc, k, name=name).text = str(text)
    tree = etree.ElementTree(root)
    tree.write(outxml, pretty_print=True)


def read_grid_metadata(xml, as_dict=False):
    """
    Reads the associated metadata.xml file which contains bounding-box coordinates, nrow, ncol, etc.
    """
    from lxml import etree
    results = OrderedDict()
    tree = etree.parse(xml)
    root = tree.getroot()
    for child_of_root in root:
        for child_of_subelement in child_of_root:
            if child_of_subelement.tag in ['ul', 'ur', 'lr', 'll']:
                results[child_of_subelement.tag] = literal_eval(child_of_subelement.text)
            elif child_of_subelement.tag in ['delx', 'dely']:
                results[child_of_subelement.tag] = float(child_of_subelement.text)
            elif child_of_subelement.tag in ['nrow', 'ncol']:
                results[child_of_subelement.tag] = int(child_of_subelement.text)
            elif child_of_subelement.tag == 'rotation':
                results[child_of_subelement.tag] = float(child_of_subelement.text)
            else:
                results[child_of_subelement.tag] = child_of_subelement.text
                # print('Metadata tag not recognized:', child_of_subelement.tag, 'value:', child_of_subelement.text)
    if as_dict:
        return results
    else:
        return results.values()


# def get_proj_coords(x_coords=os.path.join(gridpath, 'x_coords.npy'), y_coords=os.path.join(gridpath, 'y_coords.npy')):
#     """
#     Get the projected coordinates for the input MODFLOW grid.
#     :param x_coords: location of a numpy array file containing x-coordinates
#     :param y_coords: location of a numpy array file containing y-coordinates
#     :return: x, y arrays
#     """
#     x = np.load(x_coords)
#     y = np.load(y_coords)
#     return x, y
#
#
# def get_latlons(lats=os.path.join(gridpath, 'lat.npy'), lons=os.path.join(gridpath, 'lon.npy')):
#     """
#     Get the lat/lons for the input MODFLOW grid.
#     :param lats: location of a numpy array file containing latitudes
#     :param lons: location of a numpy array file containing longitudes
#     :return: lat, lon arrays
#     """
#     lat = np.load(lats)
#     lon = np.load(lons)
#     return lat, lon


def read_package_data(nrow, ncol, file_in, kw, dtype=np.float):
    data = np.zeros((nrow * ncol), dtype=dtype) + np.NaN
    with open(file_in, 'r') as f:
        while True:
            line = f.readline()
            if kw.lower() in line.lower():
                break
        d = 0
        while True:
            line = f.readline()
            for a in line.split():
                try:
                    data[d] = dtype(a)
                except:
                    raise Exception('unable to cast value: ' +
                        str(a) + ' to type:' + str(dtype))
                d += 1
            if d == nrow*ncol or not line:
                break
    data.resize(nrow, ncol)
    return data


def array_to_ascii_grid(in_array, out_file_prefix, ul_x_corner, ul_y_corner, cell_size, no_data_val=-9999.):
    """
    Write a numpy array out to an ESRI ascii grid.
    :param in_array: 2- or 3-dimensional array
    :param out_file_prefix:
    :param ul_x_corner:
    :param ul_y_corner:
    :param cell_size:
    :param no_data_val:
    :return:
    """
    if not isinstance(no_data_val, str):
        try:
            no_data_val = str(no_data_val)
        except Exception as e:
            print(e)
            print('Could not cast no_data_val', no_data_val, 'to string.')
    """
    Write out a grid in ESRI Arc ASCII format
    """
    if len(in_array.shape) == 2:
        nrow, ncol = in_array.shape
        out_files = [out_file_prefix + '.asc']
        in_arrays = [in_array]
    elif len(in_array.shape) == 3:
        nrow, ncol = in_array.shape[1:]
        out_files = [out_file_prefix + '_Layer_{}'.format(lay) + '.asc' for lay in range(in_array.shape[0])]
        in_arrays = [in_array[i, :, :] for i in range(in_array.shape[0])]

    for a, fname in zip(in_arrays, out_files):

        with open(fname, 'w') as f:
            f.write("NCOLS         {0}\n".format(ncol))
            f.write("NROWS         {0}\n".format(nrow))
            f.write("XLLCORNER     {0}\n".format(ul_x_corner))
            f.write("YLLCORNER     {0}\n".format(ul_y_corner))
            f.write("CELLSIZE      {0}\n".format(cell_size))
            f.write("NODATA_VALUE  {0}\n".format(no_data_val))

        a[np.isnan(a)] = no_data_val

        # If the array has a mask, fill it with the nodata value
        if isinstance(a, np.ma.MaskedArray):
            try:
                a = np.ma.filled(a, no_data_val)
            except Exception as e:
                print(e)

        # reopen file in 'append' mode and pass this filehandle to np.savetxt
        with open(fname, 'a') as f:
            if a.dtype == np.int:
                np.savetxt(f, a, delimiter=" ", fmt="%d")
            else:
                np.savetxt(f, a, delimiter=" ", fmt="%15.6E")

    return


def clean_ws(ws, subdirs=(), ignorefiles=(), ignoreext=('bat', 'lnk', 'exe', 'ipynb', 'py')):
    for ext in ignoreext:
        ignorefiles += tuple(glob(os.path.join(ws, '*.{}'.format(ext))))
    assert os.path.isdir(ws), 'Path does not exist: {}'.format(ws)
    for i, ifile in enumerate(ignorefiles):
        if '*' in ifile:
            ignorefiles.pop(i)
            ignorefiles.extend([glob(ifile)])
    for root, dirs, files in os.walk(ws):
        if root == ws or os.path.basename(root) in subdirs:
            print('Clearing {}'.format(root))
            for file in files:
                f = os.path.join(root, file)
                if f in ignorefiles:
                    continue
                try:
                    os.remove(f)
                except:
                    print('File could not be removed:', f)
    return


def zip_file(src, dst):
    """
    Zip a single file or a list of files
    """
    if isinstance(src, str):
        src = [src]
    zipf = ZipFile(dst, 'w', ZIP_DEFLATED, True)
    for f in src:
        print('Adding file to archive "{}":'.format(dst), f)
        zipf.write(f)
    zipf.close()
    return


def zip_directory(src, dst, ignore_dirs=[], ignore_files=[]):
    """
    Zip a whole folder and all subdirectories
    """

    # Find list of dirs to ignore
    ignore = []
    for d in ignore_dirs:
        for root, dirs, files in os.walk(d):
            ignore.append(root)
    zipf = ZipFile(dst, 'w', ZIP_DEFLATED, allowZip64=True)
    for root, dirs, files in os.walk(src):
        if root not in ignore:
            print(os.path.join(src, root))
            for file in files:
                if os.path.join(root, file) not in ignore_files:
                    print('\t', file)
                    zipf.write(os.path.join(root, file))
    zipf.close()
    return


def copy_directory(src, dst='.', ignore_dirs=[], ignore_files=[]):
    """
    Copy a whole folder and all subdirectories
    """

    # Find list of dirs to ignore
    ignore = []
    for d in ignore_dirs:
        for root, dirs, files in os.walk(d):
            ignore.append(root)
    for root, dirs, files in os.walk(src):
        if root not in ignore:
            print(root)
            for file in files:
                if os.path.join(root, file) not in ignore_files:
                    src_f = os.path.join(root, file)
                    dst_f = os.path.join(dst, file)
                    print('\t', src_f, dst_f)
                    shutil.copy2(src_f, dst_f)
    return


def update_array_by_node(a, nodes, values):
    if isinstance(nodes, list):
        nodes = np.array(nodes)
    if isinstance(values, list):
        values = np.array(values)
    assert nodes.max() < a.size, 'Node values are out of range for array of size {}'.format(a.size)
    assert len(nodes) == len(values), 'Length of nodes and values arrays is inconsistent.'
    b = a.ravel()
    b[nodes] = values
    c = b.reshape(a.shape)
    return c


def neighbor_mean(a, size=3):
    """
    Computes the mean value of neighbors in an array.
    a : numpy array
        The array for which neighbor-means will be computed
    size : int (odd)
        The size of the footprint array to use in defining how many neighbors to capture
    :return : numpy array
    """
    s = 'Please choose an odd integer greater' \
        ' than 1 for the "size" argument.'
    assert size > 1 and not size % 2 == 0, s
    mask = np.ones((size, size))
    c = math.floor(size / 2)
    mask[c, c] = 0
    result = generic_filter(a, np.nanmean, footprint=mask, size=size, mode='constant', cval=np.NaN)
    return result

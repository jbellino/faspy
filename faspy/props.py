
# Hydraulic Properties
# --------------------

# Generic sand
# ------------
# Page 85, C.W. Fetter, average HK of well-sorted sands; 1 cm/s = 2835 ft/day
# kx_sand = 10.e-2 * 2835.
# Average HK of surficial aquifer system in SW Fla. reported by Knochenmus (2006, pg. 11)
kx_sand = 62.
kz_sand = kx_sand * .1


# Generic clay
# ------------
# Page 85, C.W. Fetter, average HK of clay, fine sands; 1 cm/s = 2835 ft/day
kx_clay = 10.e-8 * 2835.
kz_clay = kx_clay*.01


# Generic limestone
# -----------------
# Page 764, Langevin (2003), HK zone 1
# kx_lmsn = 9000./3.2808
kx_lmsn = 500.                 # Arbitrary.
kz_lmsn = kx_lmsn*.1


# Generic tight limestone
# -----------------------
kx_lmsn_tight = kx_lmsn*.01
kz_lmsn_tight = kz_lmsn*.01


# Generic cavernous limestone
# ---------------------------
kx_lmsn_cvrn = kx_lmsn*10.
kz_lmsn_cvrn = kx_lmsn_cvrn    # Assume isotropic conditions


# Generic fractured limestone
# ---------------------------
kz_lmsn_frac = kx_lmsn_cvrn    # Assume isotropic conditions


# Intermediate aquifer system
# ---------------------------
# Values as reported by Knochenmus (2006, pg. 35-36)
# kx_ias = kx_lmsn*.01
kx_ias = kx_lmsn


# Sand and gravel aquifer
# -----------------------
# Franks (1988) notes that the calibrated K values for model layers 1 and 2 was varied over the entire
# likely range of values (9-27 m/d) (pg. 50). Model fit was poor, likely owing to local heterogeneities
# and the model was found to be highly sensitive to leakace between layers 1 and 2. The final calibrated
# conductivity value used for layer 3 (main production zone) was 30 m/d (pg. 50).
kx_sandgrav = ((980/20) * 3.2808)   # Franks (1988) used 980 m2/d (pg. 48) for production zone T (20m thick, pg. 29)
kz_sandgrav = kx_sandgrav * .1

__author__ = 'jbellino'
"""
Sets some default font parameters to be applied to all figures.
See http://matplotlib.org/users/customizing.html for a complete guide to the rc parameter defaults.
"""

import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import inspect
# print 'Loaded', inspect.getfile(inspect.currentframe())

# import matplotlib.font_manager as fm

rcdef = plt.rcParams.copy()

fontsize = 8
axislabelsize = 9
ticklabelsize = 8
linewidth = .5

mpl.rcParams['font.size']           = fontsize
mpl.rcParams['font.sans-serif']     = 'Univers 57 Condensed' # 'Arial' #
mpl.rcParams['font.serif']          = 'Times'
mpl.rcParams['font.cursive']        = 'Zapf Chancery'
mpl.rcParams['font.fantasy']        = 'Comic Sans MS'
mpl.rcParams['font.monospace']      = 'Courier New'
mpl.rcParams['mathtext.default']    = 'regular'
mpl.rcParams['pdf.compression']     = 0
mpl.rcParams['pdf.fonttype']        = 42

mpl.rcParams['xtick.labelsize'] = ticklabelsize
mpl.rcParams['ytick.labelsize'] = ticklabelsize

mpl.rcParams['lines.linewidth']   = linewidth
mpl.rcParams['patch.linewidth']   = linewidth
mpl.rcParams['xtick.major.width'] = linewidth
mpl.rcParams['xtick.minor.width'] = linewidth

# The figure subplot parameters.  All dimensions are a fraction of the
# figure width or height
mpl.rcParams['figure.subplot.left']     = 0.125
mpl.rcParams['figure.subplot.right']    = 0.9
mpl.rcParams['figure.subplot.bottom']   = 0.1
mpl.rcParams['figure.subplot.top']      = 0.9
mpl.rcParams['figure.subplot.wspace']   = 0.2
mpl.rcParams['figure.subplot.hspace']   = 0.2

mpl.rcParams['figure.dpi']              = 150.
mpl.rcParams['figure.autolayout']       = False
mpl.rcParams['figure.max_open_warning'] = 40

mpl.rcParams['legend.fontsize'] = fontsize
mpl.rcParams['legend.frameon']  = False

mpl.rcParams['axes.labelsize'] = axislabelsize
mpl.rcParams['axes.xmargin']   = .2
mpl.rcParams['axes.ymargin']   = 0.
mpl.rcParams['axes.linewidth'] = linewidth

# the default savefig params can be different from the display params
# e.g., you may want a higher resolution, or to make the figure
# background white
mpl.rcParams['savefig.dpi']         = 300.
mpl.rcParams['savefig.facecolor']   = 'white'
mpl.rcParams['savefig.edgecolor']   = 'white'
mpl.rcParams['savefig.format']      = 'png'
mpl.rcParams['savefig.bbox']        = 'tight'
mpl.rcParams['savefig.pad_inches']  = 0.1
mpl.rcParams['savefig.jpeg_quality']= 95

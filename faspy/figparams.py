# Define color palettes
light_colors = {'blue': '#6AC3F2', 'yellow': '#FFBD00', 'green': '#66A48B',
                     'bluegrey': '#91B0BD', 'lightgrey': '#C7B5BC', 'warmgrey': '#A79D95',
                     'tan': '#C19967'}

main_colors = {'green': '#006F41', 'bluegreen': '#007B71', 'blue': '#0061A3',
                'purple': '#180F9B', 'redviolet': '#8F009A', 'red': '#FB0032',
                'yellow': '#F99B00'}

websafe_colors = {'light_blue':  '#66CCFF', 'yellow': '#FFCC00', 'green_grey': '#669999',
                  'blue_grey': '#99CCCC', 'light_grey': '#CCCCCC', 'warm_grey': '#999999',
                  'tan': '#CC9966'}

fas_colors = {}

figure_sizes = {'full_page': (7.125, 8.5),
                'full_page_horizontal': (8.5, 7.125),
                'half_page_vertical': (3.5, 8.5),
                'half_page_horizonal': (7.125, 4.25),
                'three_quarter_vertical': (7.125, 4.25*1.5),
                'quarter_page': (3.5, 3.5),
                'full_page_4x4_plot': (7.125, 6.5)}

# borderwidth = .5
# linewidth = .5
# tinyfontsize = 6
# smallfontsize = 8
# mediumfontsize = 10
# largefontsize = 12
# dpi=300
# markersize_small = 8
# markersize_medium = 12

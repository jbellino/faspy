import sys
from distutils.core import setup


long_description = \
"""faspy is a cobbled-together set of modules to help me get stuff done.
"""

setup(name="faspy",
      description=long_description,
      long_description=long_description,
      author="Jason Bellino",
      author_email='jbellino@usgs.gov',
      url='https://bitbucket.org/jbellino/faspy',
      download_url = 'https://bitbucket.org/jbellino/faspy/get/master.tar.gz',
      license='New BSD',
      platforms='Windows',
      packages = ["faspy"],
      version="0.1")